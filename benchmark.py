from timeit import default_timer as timer
import numpy as np
from io import BytesIO

from pyexcelerate import Workbook

import pandas as pd
import xlsxwriter

def convert_xlsx(df):
    output = BytesIO()
    data = pd.concat([pd.DataFrame([df], columns=[clnm for clnm in df[0]]) for df in df[1:]], ignore_index=True)
    wb = Workbook()
    wb.new_sheet('data', data=data)
    wb.save('test.xlsx')
    processed_data = output.getvalue()
    return processed_data

def timing(min, max, step):
    time_df = []

    for i in range(min, max, step):
        start = timer()
        convert_xlsx(pd.DataFrame(index=np.arange(i), columns=np.arange(70)))
        print(i)
        stop = timer()
        elapsed_time = stop - start
        time_df.append([[i,elapsed_time]])
    return time_df

def to_df(bench_list):
    df = pd.DataFrame(bench_list, columns=['values'])
    df[['num', 'value']] = pd.DataFrame(df['values'].tolist(), index=df.index)
    df.drop(columns=['values'], inplace=True)
    plt.plot(df['value'], df['num'])
    plt.ylabel('Rows')
    plt.xlabel('Time')
    plt.title('My Data')
    plt.show()
