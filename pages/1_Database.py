import streamlit as st

from sqlalchemy.engine import URL
from sqlalchemy import create_engine, select, func, Column, String, MetaData, or_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker

import pandas as pd
from io import BytesIO
import xlsxwriter
import plotly.express as px

st.set_page_config(layout="wide", page_title="Remote Sensing Database")

def convert_df(df):
   return df.to_csv(index=False).encode('utf-8')


def convert_xlsx(df):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, index=False, sheet_name='Sheet1')
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']
    format1 = workbook.add_format({'num_format': '0.00'})
    worksheet.set_column('A:A', None, format1)
    writer.save()
    processed_data = output.getvalue()
    return processed_data


url_object = URL.create(
    "postgresql+psycopg2",
    username=st.secrets['postgres']['user'],
    password=st.secrets['postgres']['password'],
    host=st.secrets['postgres']['host'],
    database=st.secrets['postgres']['dbname'],
)

def init_session(url_object):
    engine = create_engine(url_object, echo=False, future=True, connect_args={'options': '-csearch_path={}'.format('rsdb')})
    Session = sessionmaker(bind=engine)
    session = Session()
    conn = session.bind.connect()
    return engine, session, conn

@st.experimental_singleton
def init_attributetable(_engine):
    Base = automap_base()

    #class VehiclePassage(Base):
    #    __tablename__ = 'VehiclePassage'
    #    __table_args__ = {'extend_existing': True}
    #
    #    SA_flag = Column('S/A flag', String)

    Base.prepare(autoload_with=_engine)
    return Base

def init_vehiclepassage(_engine):
    metadata_obj = MetaData(schema="rsdb")
    base = declarative_base(metadata=metadata_obj)

    class VehiclePassage(base):
        __tablename__ = 'VehiclePassage'
        __table_args__ = {'autoload': True,
                          'extend_existing': True,
                          'autoload_with': engine,
                          "schema": "rsdb"}
        SA_flag = Column('S/A flag', String)

    return VehiclePassage

sess_init = init_session(url_object)
engine = sess_init[0]
session = sess_init[1]
conn = sess_init[2]

VehiclePassage = init_vehiclepassage(engine)
Base = init_attributetable(engine)


FuelType = Base.classes.FuelType
AbatementTech = Base.classes.AbatementTech
SessionID = Base.classes.SessionID
Site = Base.classes.Site
EmissionStandard = Base.classes.EmissionStandard
Campaign = Base.classes.Campaign
Instrument = Base.classes.Instrument
VehicleCategory = Base.classes.VehicleCategory
VehicleMakeCode = Base.classes.VehicleMakeCode



fuel_stmt = select(FuelType)
fuel = pd.read_sql(fuel_stmt, conn)

fuel.subtype = ['Internal combustion', 'Internal combustion', 'Internal combustion', 'Electric', 'Internal combustion', 'Internal combustion', 'Internal combustion',
                'Internal combustion', 'Internal combustion', 'Internal combustion', 'Internal combustion', 'Hybrid', 'Hybrid',
                'Hybrid', 'Hybrid', 'Hybrid', 'Internal combustion', 'Internal combustion', 'Internal combustion',
                'other', 'Internal combustion', 'other', 'other']

make_stmt = select(VehicleMakeCode)
make = pd.read_sql(make_stmt, conn)

vehcat_stmt = select(VehicleCategory)
vehcat = pd.read_sql(vehcat_stmt, conn)

country_stmt = select(Site)
country = pd.read_sql(country_stmt, conn)

em_stmt = select(EmissionStandard)
em = pd.read_sql(em_stmt, conn)
session.commit()



#fuel_choice_stmt = select(FuelType).filter(FuelType.FuelTypeName.in_(fuel_choice))
with st.form('filter'):
    with st.sidebar:
        fueltype_choice = st.multiselect('Engine type: ', fuel['subtype'].unique())
        if not fueltype_choice:
            fueltype_choice = fuel['subtype'].unique()

        fuel_choice = fuel.FuelTypeName[fuel.subtype.isin(fueltype_choice)]

        fuel_specif = st.multiselect('Engine type: ', fuel.FuelTypeName[fuel.subtype.isin(fueltype_choice)])
        if not fuel_specif:
            fuel_specif = fuel.FuelTypeName.unique()

        vehcat_choice = st.multiselect('Vehicle Category: ', vehcat['VehicleCategoryName'].unique())
        if not vehcat_choice:
            vehcat_choice = vehcat['VehicleCategoryName'].unique()

        make_choice = st.multiselect('Vehicle make: ', make['VehicleMakeCodeName'].unique())
        if not make_choice:
            make_choice = make['VehicleMakeCodeName'].unique()

        emission_choice = st.multiselect('Emission standard: ', em['EmissionStandardName'].unique())
        if not emission_choice:
            emission_choice = em['EmissionStandardName'].unique()

        country_choice = st.multiselect('Country: ', country['Country'].unique())
        if not country_choice:
            country_choice = country['Country'].unique()

        municip_choice = st.multiselect('City: ', country.Municipality[country.Country.isin(country_choice)].unique())
        if not municip_choice:
            municip_choice = country.Municipality[country.Country.isin(country_choice)].unique()

        if st.checkbox("Show valid Speed/Acceleration values") is True:
            valid_check = ['V']
        else:
            valid_check = ['V', 'x', 'X', 'NA']


        session.commit()

        st.caption("Show valid measurements for:")


        def checkbox_gas_check(gas_name, disabled):
            if st.checkbox(gas_name, disabled = disabled):
                return ['V']
            else:
                return ['V', 'x', 'NA', None]


        CO_check = checkbox_gas_check("Carbon monoxide", False)
        CO2_check = checkbox_gas_check("Carbon dioxide", True)
        HC_check = checkbox_gas_check("Hydrocarbons", True)
        NOx_check = checkbox_gas_check("Nitrogen oxides", True)
        NH3_check = checkbox_gas_check("Ammonia", True)
        UV_check = checkbox_gas_check("UV Smoke", True)
        IR_check = checkbox_gas_check("IR Smoke", True)

        st.form_submit_button('Apply filter rules')




st.sidebar.button('Reset filter rules', disabled=True)

with st.form('session_filter'):
    st.text('Available Sessions:')
    session_choice = st.multiselect('Shown Sessions: ', country.SiteName[country.Municipality.isin(municip_choice)].unique(),
                                    #default=country.SiteName[country.Municipality.isin(municip_choice)].unique(),
                                    label_visibility="collapsed")
    if not session_choice:
        session_choice = country.SiteName[country.Municipality.isin(municip_choice)].unique()

    session.commit()
    st.form_submit_button('Filter by session')

#st.caption('Die angezeigten Sessions verfügen über folgende Messwerte: ')

stmt = (
    select(FuelType.FuelTypeName,
           SessionID.SiteName, EmissionStandard.EmissionStandardName, VehicleMakeCode.VehicleMakeCodeName, VehicleCategory.VehicleCategoryName,
           Site.Country, Site.Municipality, Site.longitude, Site.latitude, Site.RoadSlope_perc, VehiclePassage)
    .join_from(VehiclePassage, FuelType, VehiclePassage.FuelTypeID == FuelType.FuelTypeID)
    .join_from(VehiclePassage, SessionID, VehiclePassage.SessionID == SessionID.SessionID)
    .join_from(VehiclePassage, EmissionStandard, VehiclePassage.EmissionStandardID == EmissionStandard.EmissionStandardID)
    .join_from(VehiclePassage, VehicleMakeCode, VehiclePassage.VehicleMakeCodeID == VehicleMakeCode.VehicleMakeCodeID)
    .join_from(VehiclePassage, VehicleCategory, VehiclePassage.VehicleCategoryID == VehicleCategory.VehicleCategoryID)
    .join_from(SessionID, Site, SessionID.SiteID == Site.SiteID)
    .filter(FuelType.FuelTypeName.in_(fuel_choice))
    .filter(FuelType.FuelTypeName.in_(fuel_specif))
    .filter(EmissionStandard.EmissionStandardName.in_(emission_choice))
    .filter(VehicleCategory.VehicleCategoryName.in_(vehcat_choice))
    .filter(VehicleMakeCode.VehicleMakeCodeName.in_(make_choice))
    .filter(Site.Country.in_(country_choice))
    .filter(Site.Municipality.in_(municip_choice))
    .filter(Site.SiteName.in_(session_choice))
    .filter(or_(VehiclePassage.SA_flag.in_(valid_check), VehiclePassage.SA_flag == None))
    .filter(or_(VehiclePassage.CO_Valid.in_(CO_check), VehiclePassage.CO_Valid == None))
    .filter(or_(VehiclePassage.CO2_Valid.in_(CO2_check), VehiclePassage.CO2_Valid == None))
    .filter(or_(VehiclePassage.IR_HC_Valid.in_(HC_check), VehiclePassage.IR_HC_Valid == None))
    .filter(or_(VehiclePassage.NH3_Valid.in_(NH3_check), VehiclePassage.NH3_Valid == None))
    .filter(or_(VehiclePassage.UV_Smoke_Valid.in_(UV_check), VehiclePassage.UV_Smoke_Valid == None))
)

print(stmt.compile(compile_kwargs={"literal_binds": True}))
ct_stmt = (
    select(func.count()).select_from(stmt)
)

session_stmt = (
    select(VehiclePassage.CO_Valid, VehiclePassage.CO2_Valid, SessionID.SiteName, Site)
    .join_from(VehiclePassage, SessionID, VehiclePassage.SessionID == SessionID.SessionID)
    .join_from(SessionID, Site, SessionID.SiteID == Site.SiteID)
)

init_vp = pd.read_sql(stmt.limit(100), conn)
df_count = pd.read_sql(ct_stmt, conn)
st.header(str(df_count.values.item())+' objects found')


#SA_stmt = session.query(func.count(VehiclePassage.SA_flag), func.lower(VehiclePassage.SA_flag), SessionID.SiteName).join(SessionID, VehiclePassage.SessionID == SessionID.SessionID).group_by(func.lower(VehiclePassage.SA_flag), SessionID.SiteName)
#SA_hist = pd.read_sql(SA_stmt.statement, conn)
#SA_hist.SiteName = SA_hist.SiteName.str.replace("\\n","")
#SA_hist['lower_1'].fillna('v', inplace=True)
#SA_hist['count_1_norm'] = SA_hist.groupby('SiteName')['count_1'].transform(lambda x: x / x.sum())
#SA_hist = SA_hist[SA_hist.SiteName.isin(session_choice)]

#MT_stmt = session.query(VehiclePassage.CO_Valid,
#                        VehiclePassage.CO2_Valid,
#                        VehiclePassage.IR_HC_Valid,
#                        VehiclePassage.NO_Valid,
#                        VehiclePassage.NO2_Valid,
#                        VehiclePassage.NH3_Valid,
#                        VehiclePassage.UV_Smoke_Valid,
#                        SessionID.SiteName)\
#    .join(SessionID, VehiclePassage.SessionID == SessionID.SessionID)\
#    .group_by(VehiclePassage.CO_Valid,
#              VehiclePassage.CO2_Valid,
#              VehiclePassage.NO_Valid,
#              VehiclePassage.IR_HC_Valid,
#              VehiclePassage.NO_Valid,
#              VehiclePassage.NO2_Valid,
#              VehiclePassage.NH3_Valid,
#              VehiclePassage.UV_Smoke_Valid,
#              SessionID.SiteName)
#MT_hist = pd.read_sql(MT_stmt.statement, conn)
#MT_hist['CO_Valid'] = MT_hist['CO_Valid'].str.lower()
#MT_hist.SiteName = MT_hist.SiteName.str.replace("\\n","")
#MT_hist.groupby('CO_Valid')['CO_Valid'].value_counts()
#def rs_metrics(metric):
#    stmt = session.query(func.count(metric), func.lower(metric),
#                            SessionID.SiteName).join(SessionID,
#                                                     VehiclePassage.SessionID == SessionID.SessionID).group_by(
#        func.lower(metric), SessionID.SiteName).filter(VehiclePassage.SA_flag.in_(valid_check))
#    hist = pd.read_sql(stmt.statement, conn)
#    hist.SiteName = hist.SiteName.str.replace("\\n", "")
#    hist['lower_1'].fillna('v', inplace=True)
#    hist['count_1_norm'] = hist.groupby('SiteName')['count_1'].transform(lambda x: x / x.sum())
#    hist = hist[hist.SiteName.isin(session_choice)]
#    out_metr = str(round(hist[hist.lower_1.isin(['v'])].mean().count_1_norm,4)*100)+' %'
#    return out_metr


#mc1, mc2, mc3, mc4, mc5, mc6, mc7, mc8, mc9 = st.columns(9)
#mc1.metric(label="Valid S/A data", value=rs_metrics(VehiclePassage.SA_flag), help="Percentage of valid speed and acceleration measurements reported by the measurement")
#mc2.metric(label="Valid CO measurements", value=rs_metrics(VehiclePassage.CO_Valid))
#mc3.metric(label="Valid CO2 measurements", value=rs_metrics(VehiclePassage.CO2_Valid))
#mc4.metric(label="Valid HC measurements", value=rs_metrics(VehiclePassage.IR_HC_Valid))
#mc5.metric(label="Valid NO measurements", value=rs_metrics(VehiclePassage.NO_Valid))
#mc6.metric(label="Valid NO2 measurements", value=rs_metrics(VehiclePassage.NO2_Valid))
#mc7.metric(label="Valid NH3 measurements", value=rs_metrics(VehiclePassage.NH3_Valid))
#mc8.metric(label="Valid UV Smoke measurements", value=rs_metrics(VehiclePassage.UV_Smoke_Valid))
#mc9.metric(label="Valid PM measurements", value='NA')
## Print results.
col1, col2 = st.columns(2)

with col1:
    if st.button('Export to CSV'):
        with st.spinner(text="Exporting CSV..."):
            csv = convert_df(pd.read_sql(stmt, conn))
            st.download_button(
                "📥 CSV",
                csv,
                "file.csv",
                "text/csv",
                key='download-csv'
            )

with col2:
    if st.button('Export to XLSX'):
        st.warning('Exporting datasets with >50.000 rows may take longer than usual and is recommended for small datasets only. Consider exporting to CSV instead.', icon="⚠️")
        if st.button('Continue export'):
            with st.spinner(text="Exporting XLSX..."):
                xlsx = convert_xlsx(pd.read_sql(stmt, conn))
                st.download_button(
                    "📥 XLSX",
                    data=xlsx,
                    file_name="file.xlsx",
                    key='download-xlsx'
                )
        else:
            if st.button('Abort export'):
                st.stop()

st.dataframe(init_vp)


#col1, col2 = st.columns(2)
#
#fig = px.bar(SA_hist, x="count_1_norm", y="SiteName", color="lower_1",
#            hover_data=['count_1_norm'], barmode = 'stack')
#col1.plotly_chart(fig, theme="streamlit", use_container_width=True)
