import streamlit as st
from streamlit_timeline import st_timeline

from sqlalchemy.engine import URL
from sqlalchemy import create_engine, select, func, Column, String, MetaData, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker, aliased

import pandas as pd
import ast
from io import BytesIO

from deepdiff import DeepDiff


st.set_page_config(layout="wide", page_title="Changelog")

url_object = URL.create(
    "postgresql+psycopg2",
    username=st.secrets['postgres']['user'],
    password=st.secrets['postgres']['password'],
    host=st.secrets['postgres']['host'],
    database=st.secrets['postgres']['dbname'],
)

def init_session(url_object):
    engine = create_engine(url_object, echo=True, future=True, connect_args={'options': '-csearch_path={}'.format('logging')})
    Session = sessionmaker(bind=engine)
    session = Session()
    conn = session.bind.connect()
    return engine, session, conn

def init_txlog(_engine):
    metadata_obj = MetaData(schema="logging")
    Base = automap_base()

    # class VehiclePassage(Base):
    #    __tablename__ = 'VehiclePassage'
    #    __table_args__ = {'extend_existing': True}
    #
    #    SA_flag = Column('S/A flag', String)

    Base.prepare(autoload_with=_engine)
    return Base

def init_attr_session(url_object):
    engine = create_engine(url_object, echo=True, future=True, connect_args={'options': '-csearch_path={}'.format('rsdb')})
    Session = sessionmaker(bind=engine)
    session = Session()
    conn = session.bind.connect()
    return engine, session, conn


sess_init = init_session(url_object)
engine = sess_init[0]
session = sess_init[1]
conn = sess_init[2]

attr_init = init_attr_session(url_object)
engine_attr = attr_init[0]
session_attr = attr_init[1]
conn_attr = attr_init[2]

def init_attributetable(_engine):
    Base = automap_base()

    #class VehiclePassage(Base):
    #    __tablename__ = 'VehiclePassage'
    #    __table_args__ = {'extend_existing': True}
    #
    #    SA_flag = Column('S/A flag', String)

    Base.prepare(autoload_with=_engine)
    return Base

Base = init_attributetable(engine_attr)
SessionID = Base.classes.SessionID
Site = Base.classes.Site

session_stmt = (
    select(SessionID.SessionID, SessionID.SiteName, Site.SiteID, Site.SiteName)
    .join_from(SessionID, Site, SessionID.SiteID == Site.SiteID)
)

sessions = pd.read_sql(session_stmt, conn_attr)

t_history = init_txlog(engine)
changelog = t_history.classes.t_history

cset_stmt = select(func.distinct(changelog.tstamp), func.dense_rank().over(order_by=(changelog.tstamp)).label('changeset'))

#table for the amount of changed lines for each changeset
cset_hist = pd.read_sql(cset_stmt, conn)
cset_hist.distinct_1 = cset_hist.distinct_1.apply(lambda x: x.strftime('%Y-%m-%d %X'))

#reformat table for the st_timeline plugin
cset_hist = cset_hist.rename(columns={'distinct_1': 'start', 'changeset': 'content'})
cset_hist.insert(0, 'id', range(1, 1 + len(cset_hist)))
cset_hist['content'] = cset_hist['content'].astype(str)







#st.dataframe(cset_hist, use_container_width=True)

#display interactive changeset timeline
timeline = st_timeline(cset_hist.to_dict('records'), groups=[], options={}, height="300px")


st.write(timeline)
print(timeline['content'])

hist_stmt = select(changelog, func.dense_rank().over(order_by=(changelog.tstamp)).label('changeset')).subquery()
hist_subquery = select(hist_stmt).filter(hist_stmt.c.changeset.in_([timeline['content']]))
print(hist_subquery)
#.filter(func.dense_rank().over(order_by=(changelog.tstamp)).label('changeset').in_(timeline['content']))
init_hist = pd.read_sql(hist_subquery, conn)
init_hist['change'] = init_hist.apply(lambda x: DeepDiff(x.old_val, x.new_val, view='tree'), axis=1)
init_hist['change_keys'] = init_hist.change.apply(lambda x: list(x.t2.keys()) if isinstance(x.t2, dict) else None)
init_hist['SessionID'] = init_hist.new_val.apply(lambda x: x['SessionID'])
init_hist = init_hist.merge(sessions[['SessionID', 'SiteName']], on="SessionID")

if init_hist.old_val.count() == 0:
    init_hist.new = True
#changesets = init_hist.drop_duplicates(subset=['changeset'], keep='last')[['changeset', 'tstamp', 'tabname', 'operation', 'change_keys', 'SiteName']]
#changesets = init_hist[['changeset', 'tstamp', 'tabname', 'operation', 'SiteName']].agg(lambda x: x.unique().tolist())
changesets = init_hist.groupby('changeset').agg({'SiteName':lambda x: x.unique().tolist(),
                                                'SessionID':lambda x: x.unique().tolist(),
                                                 'tstamp': lambda x: x.unique().tolist(),
                                                 'tabname': lambda x: x.unique().tolist(),
                                                 'operation': lambda x: x.unique().tolist()})
changesets['change_keys'] = str(init_hist.change_keys.explode().unique().tolist())
changesets['change_keys'] = changesets['change_keys'].apply(ast.literal_eval)

#changesets = init_hist.groupby('changeset').size().to_frame().rename(columns={0: 'changed lines'}).reset_index().merge(changesets, on='changeset', how='left')
#changeset_hist = pd.read_sql(changeset_stmt, conn)

with st.spinner(text="generating changeset metadata..."):
    st.dataframe(changesets, use_container_width=True)