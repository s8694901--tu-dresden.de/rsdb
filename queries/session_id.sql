create table rsdb."SessionID"
(
    "SessionID"      INT
        primary key,
    "CampaignID"     INT,
    "SiteID"         INT,
    "SiteName"       TEXT,
    "InstrumentID"   INT,
    "InstrumentName" TEXT,
    "Starttime"      timestamp,
    "Stoptime"       timestamp
);
