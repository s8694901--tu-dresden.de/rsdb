SELECT a."VehiclePassageID",
       a."PassageTime",
       a."S/A flag",
       a."Speed kph",
       a."Accel kphpersec",
       a.vsp,
       a."ValidPlumePoints",
       a."AverageCO2",
       a."Max_CO2",
       --PPM-Werte
       a."Percent_CO",
       a."Percent_CO2",
       a."PPM_HC_Hexane",
       a."PPM_HC_Propane",
       a.ppm_no,
       a.ppm_no2,
       a.ppm_nh3,
       a."UV_Smoke",
       a."IR_Smoke",

       a."VSPStatus",
       a."AmbientTemperature",
       a."BarometricPressure",
       a."Humidity",
       a."CO_gpkg",
       a."CO2_gpkg",
       a."HC_gpkg",
       a."NO_gpkg",
       a."NO2_gpkg",
       a."NOx_gpkg",
       a."NH3_gpkg",
       a."UV_Smoke_gpkg",
       a."IR_Smoke_gpkg",
       a."CO_Valid",
       a."CO2_Valid",
       a."IR_HC_Valid",
       a."NO_Valid",
       a."NO2_Valid",
       a."NH3_Valid",
       a."UV_Smoke_Valid",
       a."NO2 measured or estimated",
       MD5(a.licence) as licence,
       LEFT(a."VIN/CHASSIS NUMBER",11) AS VIN,
       a."VEHICLE DESCRIPTION",
       a."TRADE DESIGNATION",
       a."MODEL YEAR",
       a."MONTH OF MANUFACTURE",
       a."DATE OF REGISTRATION",
       a."DISPLACEMENT_cm3",
       a."CURB WEIGHT_kg",
       a."TOTAL WEIGHT_kg",
       a."POWER_FUEL1_kW",
       a."WIDTH_mm", --expert
       a."HEIGHT_mm", --expert
       a."LENGTH_mm", --expert
       a."CERT_CO2_gkm_MIXED", --expert
       a."CERT_CO2_gkm_RURALMOTORWAY", --expert
       a."CERT_CO2_gkm_URBAN", --expert
       a."Mileage_km",
       a."Inspection date",
       a."VehiclePassageComments",
       a."Segment", --expert1
       a."On-road mass", --expert1
       a."FF_R0_N/t", --expert1
       a."FF_R1_(Ns/m)/t", --expert1
       a."FF_cw*A_m2/t", --expert1
       a."FF_coeff_A", --expert1
       a."FF_coeff_B", --expert1
       a."FF_coeff_C", --expert1
       a."FF_flag", --expert1
       a.pfcode,
       a.getriebe,
       a."VSPHausberger",

       e."EmissionStandardName",
       s."SiteName",
       --s."InstrumentName",
       f."FuelTypeName",
       ab."AbatementTechName",
       vm."VehicleMakeCodeName"
FROM rsdb."VehiclePassage" a



--Referenztabellen
FULL JOIN rsdb."EmissionStandard" e ON a."EmissionStandardID" = e."EmissionStandardID"
FULL JOIN rsdb."SessionID" s ON a."SessionID" = s."SessionID"
FULL JOIN rsdb."VehicleMakeCode" vm ON a."VehicleMakeCodeID" = vm."VehicleMakeCodeID"
FULL JOIN rsdb."FuelType" f ON a."FuelTypeID" = f."FuelTypeID"
FULL JOIN rsdb."AbatementTech" ab ON a."AbatementTechID" = ab."AbatementTechID"
