import streamlit as st
import psycopg2
from io import BytesIO
import plotly.express as px
import xlsxwriter
import pandas as pd


st.set_page_config(layout="wide")

# Initialize connection.
# Uses st.experimental_singleton to only run once.
@st.experimental_singleton
def init_connection():
    return psycopg2.connect(**st.secrets["postgres"])

conn = init_connection()

# Perform query.
# Uses st.experimental_memo to only rerun when the query changes or after 10 min.
@st.experimental_memo(ttl=600)
def run_query(query):
    with conn.cursor() as cur:
        cur.execute(query)
        return cur.fetchall()

def convert_df(df):
   return df.to_csv(index=False).encode('utf-8')

def convert_xlsx(df):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, index=False, sheet_name='Sheet1')
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']
    format1 = workbook.add_format({'num_format': '0.00'})
    worksheet.set_column('A:A', None, format1)
    writer.save()
    processed_data = output.getvalue()
    return processed_data


session_query = """SELECT DISTINCT("SiteName") FROM rsdb."SessionID";"""
session2_query = """SELECT * FROM rsdb."SessionID";"""
em_query = """SELECT * FROM rsdb."EmissionStandard";"""
fuel_query = """SELECT * FROM rsdb."FuelType";"""
site_query = """SELECT * FROM rsdb."Site";"""
city_query = """SELECT * FROM rsdb."Site";"""
vehcat_query = """SELECT * FROM rsdb."VehicleCategory";"""




df_em = pd.read_sql_query(em_query, conn)
df_fuel = pd.read_sql_query(fuel_query, conn)
df_session = pd.read_sql_query(session_query, conn)
df_session2 = pd.read_sql_query(session2_query, conn)
df_vehcat = pd.read_sql_query(vehcat_query, conn)

df_site = pd.read_sql_query(site_query, conn)
df_city = pd.read_sql_query(city_query, conn)


#introduce subcategories for easier fuel type indexing
fuel_subtype = ['Benzin', 'Diesel', 'Gas', 'EV', 'Gas', 'Diesel sonst.', 'Wasserstoff',
                'Benzin', 'Benzin bivalent', 'FlexFuel', 'FlexFuel', 'Plug-In Hybrid', 'Plug-In Hybrid',
                'Hybrid', 'Hybrid', 'Hybrid', 'FlexFuel', 'FlexFuel', 'Benzin bivalent',
                'other', 'Diesel bivalent', 'other', 'other']

df_fuel['subtype'] = fuel_subtype

#emission standard subcategories
es_subtype = ['other', 'other', 'Euro 0', 'Euro 1', 'Euro 2', 'Euro 3', 'Euro 4', 'Euro 5', 'Euro EEV', 'Euro 6', 'Euro 6', 'Euro 6', 'Euro 6', 'Euro 6']
df_em['subtype'] = es_subtype

#SITE_choice = st.sidebar.selectbox('Site name: ', df_session['SiteName'])
ES_choice = st.sidebar.multiselect('Emission Standard: ', df_em['subtype'].unique(), default=df_em['subtype'].unique()) #mit subgroups arbeiten!
ES_query = df_em[df_em['subtype'].isin(ES_choice)].EmissionStandardName.to_list()

FU_choice = st.sidebar.multiselect('Fuel type: ', df_fuel['subtype'].unique(), default=df_fuel['subtype'].unique())
FU_query = df_fuel[df_fuel['subtype'].isin(FU_choice)].FuelTypeName.to_list()
print(FU_query)

#country selection
country_choice = st.sidebar.multiselect('Land: ', df_site.Country.unique(), default=df_site.Country.unique())

#VehCat selection
vehcat_selection = st.sidebar.multiselect('Vehicle Category: ', df_vehcat.VehicleCategoryName.unique(), default=df_vehcat.VehicleCategoryName.unique())

#year selection
df_session2['year'] = pd.DatetimeIndex(df_session2['Starttime']).year
country_year = df_site[df_site.Country.isin(country_choice)].SiteID.to_list()
session_year = df_session2[df_session2.SiteID.isin(country_year)].year.drop_duplicates().sort_values()
year_choice = st.sidebar.multiselect('Kampagnenjahr: ', session_year, default=session_year)
print(year_choice)

#city selection
country_year_siteid = df_session2[df_session2.SiteID.isin(country_year)].SiteID.unique().tolist()
city = df_city[df_city.SiteID.isin(country_year_siteid)].Municipality.unique()
city_choice = st.sidebar.multiselect('Stadt: ', city, default=city)
print(city_choice)


##year selection
#df_session2['year'] = pd.DatetimeIndex(df_session2['Starttime']).year
#year_choice = st.sidebar.multiselect('Campaign year: ', df_session2['year'].drop_duplicates().sort_values())

##country selection
#siteID_year = df_session2[df_session2.year.isin(year_choice)].SiteID.values.tolist()
#country_year = df_site[df_site['SiteID'].isin(siteID_year)].Country
#country_choice = st.sidebar.multiselect('Country: ', country_year.unique(), default=country_year.unique())

##city selection
#city_year = df_city[df_city["Country"].isin(country_choice)].Municipality.unique()
#city_choice = st.sidebar.multiselect('City: ', city_year, default=city_year)



#count_query = """SELECT COUNT(*) FROM rsdb."VehiclePassage" WHERE s."SiteName" = '%s' AND e."EmissionStandardName" = '%s' AND f."FuelTypeName" IN %s""" % (SITE_choice, ES_choice, tuple(FU_choice))

query = """SELECT 
       e."EmissionStandardName",
       s."SiteName",
       si."Country",
       --s."InstrumentName",
       f."FuelTypeName",
       ab."AbatementTechName",
       vm."VehicleMakeCodeName",
       vc."VehicleCategoryName",

       a."VehiclePassageID",
       a."VehicleCategoryID",
       a."PassageTime",
       a."S/A flag",
       a."Speed kph",
       a."Accel kphpersec",
       a.vsp,
        --PPM-Werte
       a."Percent_CO",
       a."Percent_CO2",
       a."PPM_HC_Hexane",
       a."PPM_HC_Propane",
       a.ppm_no,
       a.ppm_no2,
       a.ppm_nh3,
       a."VSPStatus",
       a."AmbientTemperature",
       a."BarometricPressure",
       a."Humidity",
       a."CO_gpkg",
       a."CO2_gpkg",
       a."HC_gpkg",
       a."NO_gpkg",
       a."NO2_gpkg",
       a."NOx_gpkg",
       a."NH3_gpkg",
       a."UV_Smoke_gpkg",
       a."IR_Smoke_gpkg",
       a."CO_Valid",
       a."CO2_Valid",
       a."IR_HC_Valid",
       a."NO_Valid",
       a."NO2_Valid",
       a."NH3_Valid",
       a."UV_Smoke_Valid",
       a."NO2 measured or estimated",
       MD5(a.licence) as licence,
       LEFT(a."VIN/CHASSIS NUMBER",11) AS VIN,
       a."MODEL NAME",
       a."VEHICLE DESCRIPTION",
       a."TRADE DESIGNATION",
       a."MODEL YEAR",
       a."MONTH OF MANUFACTURE",
       a."DATE OF REGISTRATION",
       a."DISPLACEMENT_cm3",
       a."CURB WEIGHT_kg",
       a."TOTAL WEIGHT_kg",
       a."POWER_FUEL1_kW",
       a."WIDTH_mm", --expert
       a."HEIGHT_mm", --expert
       a."LENGTH_mm", --expert
       a."CERT_CO2_gkm_MIXED", --expert
       a."CERT_CO2_gkm_RURALMOTORWAY", --expert
       a."CERT_CO2_gkm_URBAN", --expert
       a."Mileage_km",
       a."Inspection date",
       a."VehiclePassageComments",
       a."Segment", --expert1
       a."On-road mass", --expert1
       a."FF_R0_N/t", --expert1
       a."FF_R1_(Ns/m)/t", --expert1
       a."FF_cw*A_m2/t", --expert1
       a."FF_coeff_A", --expert1
       a."FF_coeff_B", --expert1
       a."FF_coeff_C", --expert1
       a."FF_flag", --expert1
       a.pfcode,
       a.getriebe,
       a."VSPHausberger"


FROM rsdb."VehiclePassage" a




FULL JOIN rsdb."EmissionStandard" e ON a."EmissionStandardID" = e."EmissionStandardID"
FULL JOIN rsdb."SessionID" s ON a."SessionID" = s."SessionID"
FULL JOIN rsdb."Site" si ON s."SiteID" = si."SiteID"
FULL JOIN rsdb."VehicleMakeCode" vm ON a."VehicleMakeCodeID" = vm."VehicleMakeCodeID"
FULL JOIN rsdb."FuelType" f ON a."FuelTypeID" = f."FuelTypeID"
FULL JOIN rsdb."AbatementTech" ab ON a."AbatementTechID" = ab."AbatementTechID"
FULL JOIN rsdb."VehicleCategory" vc ON a."VehicleCategoryID" = vc."VehicleCategoryID"


WHERE e."EmissionStandardName" = ANY(%s) AND f."FuelTypeName" = ANY(%s)
AND vc."VehicleCategoryName" = ANY(%s) 
AND a."S/A flag" = ANY(%s)
AND EXTRACT(YEAR from "PassageTime") = ANY(%s) 
AND si."Country" = ANY(%s)
AND si."Municipality" = ANY(%s)
LIMIT 1000;"""

full_query = """SELECT 
       e."EmissionStandardName",
       s."SiteName",
       si."Country",
       --s."InstrumentName",
       f."FuelTypeName",
       ab."AbatementTechName",
       vm."VehicleMakeCodeName",
       vc."VehicleCategoryName",

       a."VehiclePassageID",
       a."VehicleCategoryID",
       a."PassageTime",
       a."S/A flag",
       a."Speed kph",
       a."Accel kphpersec",
       a.vsp,
        --PPM-Werte
       a."Percent_CO",
       a."Percent_CO2",
       a."PPM_HC_Hexane",
       a."PPM_HC_Propane",
       a.ppm_no,
       a.ppm_no2,
       a.ppm_nh3,
       a."VSPStatus",
       a."AmbientTemperature",
       a."BarometricPressure",
       a."Humidity",
       a."CO_gpkg",
       a."CO2_gpkg",
       a."HC_gpkg",
       a."NO_gpkg",
       a."NO2_gpkg",
       a."NOx_gpkg",
       a."NH3_gpkg",
       a."UV_Smoke_gpkg",
       a."IR_Smoke_gpkg",
       a."CO_Valid",
       a."CO2_Valid",
       a."IR_HC_Valid",
       a."NO_Valid",
       a."NO2_Valid",
       a."NH3_Valid",
       a."UV_Smoke_Valid",
       a."NO2 measured or estimated",
       MD5(a.licence) as licence,
       LEFT(a."VIN/CHASSIS NUMBER",11) AS VIN,
       a."MODEL NAME",
       a."VEHICLE DESCRIPTION",
       a."TRADE DESIGNATION",
       a."MODEL YEAR",
       a."MONTH OF MANUFACTURE",
       a."DATE OF REGISTRATION",
       a."DISPLACEMENT_cm3",
       a."CURB WEIGHT_kg",
       a."TOTAL WEIGHT_kg",
       a."POWER_FUEL1_kW",
       a."WIDTH_mm", --expert
       a."HEIGHT_mm", --expert
       a."LENGTH_mm", --expert
       a."CERT_CO2_gkm_MIXED", --expert
       a."CERT_CO2_gkm_RURALMOTORWAY", --expert
       a."CERT_CO2_gkm_URBAN", --expert
       a."Mileage_km",
       a."Inspection date",
       a."VehiclePassageComments",
       a."Segment", --expert1
       a."On-road mass", --expert1
       a."FF_R0_N/t", --expert1
       a."FF_R1_(Ns/m)/t", --expert1
       a."FF_cw*A_m2/t", --expert1
       a."FF_coeff_A", --expert1
       a."FF_coeff_B", --expert1
       a."FF_coeff_C", --expert1
       a."FF_flag", --expert1
       a.pfcode,
       a.getriebe,
       a."VSPHausberger"


FROM rsdb."VehiclePassage" a




FULL JOIN rsdb."EmissionStandard" e ON a."EmissionStandardID" = e."EmissionStandardID"
FULL JOIN rsdb."SessionID" s ON a."SessionID" = s."SessionID"
FULL JOIN rsdb."Site" si ON s."SiteID" = si."SiteID"
FULL JOIN rsdb."VehicleMakeCode" vm ON a."VehicleMakeCodeID" = vm."VehicleMakeCodeID"
FULL JOIN rsdb."FuelType" f ON a."FuelTypeID" = f."FuelTypeID"
FULL JOIN rsdb."AbatementTech" ab ON a."AbatementTechID" = ab."AbatementTechID"
FULL JOIN rsdb."VehicleCategory" vc ON a."VehicleCategoryID" = vc."VehicleCategoryID"


WHERE e."EmissionStandardName" = ANY(%s) AND f."FuelTypeName" = ANY(%s) 
AND vc."VehicleCategoryName" = ANY(%s)
AND a."S/A flag" = ANY(%s)
AND EXTRACT(YEAR from "PassageTime") = ANY(%s) 
AND si."Country" = ANY(%s)
AND si."Municipality" = ANY(%s);"""


count_query = """SELECT COUNT(*) FROM (
SELECT
COALESCE(a."EmissionStandardID",9) AS "EmissionStandardID",
a."VehicleCategoryID",
e."EmissionStandardName",
s."SiteName",
si."Country",
si."Municipality",
--s."InstrumentName",
f."FuelTypeName",
ab."AbatementTechName",
vm."VehicleMakeCodeName",
vc."VehicleCategoryName"
FROM rsdb."VehiclePassage" a

--Referenztabellen
FULL JOIN rsdb."EmissionStandard" e ON a."EmissionStandardID" = e."EmissionStandardID"
FULL JOIN rsdb."SessionID" s ON a."SessionID" = s."SessionID"
FULL JOIN rsdb."Site" si ON s."SiteID" = si."SiteID"
FULL JOIN rsdb."VehicleMakeCode" vm ON a."VehicleMakeCodeID" = vm."VehicleMakeCodeID"
FULL JOIN rsdb."FuelType" f ON a."FuelTypeID" = f."FuelTypeID"
FULL JOIN rsdb."AbatementTech" ab ON a."AbatementTechID" = ab."AbatementTechID"
FULL JOIN rsdb."VehicleCategory" vc ON a."VehicleCategoryID" = vc."VehicleCategoryID"

WHERE e."EmissionStandardName" = ANY(%s) AND f."FuelTypeName" = ANY(%s) 
AND vc."VehicleCategoryName" = ANY(%s)
AND a."S/A flag" = ANY(%s)
AND EXTRACT(YEAR from "PassageTime") = ANY(%s) 
AND si."Country" = ANY(%s)
AND si."Municipality" = ANY(%s)) AS c"""

make_query = """SELECT * FROM rsdb."VehicleMakeCode";"""




#df = pd.read_sql_query(query, conn).dropna(axis=1)
df_make = pd.read_sql_query(make_query, conn)
#results = pd.read_sql_query(count_query, conn)



#st.sidebar.selectbox('Vehicle make: ', df_make['VehicleMakeCodeName'])

print(country_choice)

#df_result = df.loc[(df['SiteName'] == SITE_choice) & (df['EmissionStandardName'] == ES_choice)] #& (df['FuelTypeName'] == FU_choice)]




#Download file

st.sidebar.checkbox("Expert")
st.sidebar.checkbox("Intermediate")
if st.sidebar.checkbox("Show data with valid S/A value") is True:
    valid_check = ['V']
else:
    valid_check = ['V', 'x', 'NA']

cur = conn.cursor()
cur.execute(query, (ES_query,FU_query,vehcat_selection,valid_check,year_choice,country_choice,city_choice))
df = pd.DataFrame(cur.fetchmany(500),
                  columns=[desc[0] for desc in cur.description])
conn.rollback()

cur = conn.cursor()
cur.execute(count_query, (ES_query, FU_query,vehcat_selection,valid_check, year_choice, country_choice, city_choice))
df_count = pd.DataFrame(cur.fetchall(),
                          columns=[desc[0] for desc in cur.description])
conn.rollback()

csv = convert_df(df)

st.subheader(str(df_count.values.item())+' objects found')

# Print results.
if st.button('Generate CSV'):
    with st.spinner(text="Generating CSV export..."):
        cur.execute(full_query, (ES_query, FU_query,vehcat_selection, valid_check, year_choice, country_choice, city_choice))
        df_complete = pd.DataFrame(cur.fetchall(),
                          columns=[desc[0] for desc in cur.description])
        conn.rollback()
        csv = convert_df(df_complete)
        st.download_button(
            "📥 CSV",
            csv,
            "file.csv",
            "text/csv",
            key='download-csv'
        )

if st.button('Generate XLSX'):
    with st.spinner(text="Generating XLSX export..."):
        cur.execute(full_query, (ES_query, FU_query,vehcat_selection, valid_check, year_choice, country_choice, city_choice))
        df_complete = pd.DataFrame(cur.fetchall(),
                                   columns=[desc[0] for desc in cur.description])
        conn.rollback()
        xlsx = convert_xlsx(df_complete)
        st.download_button(
            "📥 XLSX",
            data=xlsx,
            file_name="file.xlsx",
            key='download-xlsx'
        )

with st.spinner(text="Executing query..."):
    #df_filtered = df[df['SiteName'].isin(df_city[df_city['Municipality'].isin(city_choice)].SiteName) & df['S/A flag'].isin(valid_check)]
    df_f = df[df['S/A flag'].isin(valid_check)].head(500)
    st.dataframe(data=df_f.style.set_precision(2))
    print(df_f.head())



#kampagnen
df_campaign = pd.read_sql_query("""SELECT * FROM rsdb."Campaign";""", conn)
if st.button('Show campaigns'):
    st.dataframe(data=df_campaign)

#spielerei
col1, col2 = st.columns(2)

fig = px.histogram(df_f, x="EmissionStandardName", color="EmissionStandardName",
                   marginal="box", # or violin, rug
                   hover_data=df_f).update_xaxes(categoryorder='total ascending')
col1.plotly_chart(fig, theme="streamlit", use_container_width=True)

fig2 = px.histogram(df_f, x="VehicleMakeCodeName", color="VehicleMakeCodeName",
                   marginal="box", # or violin, rug
                   hover_data=df_f).update_xaxes(categoryorder='total ascending')
col2.plotly_chart(fig2, theme="streamlit", use_container_width=True)
#TODO
#Klassifizierung nach Land, Ort, Stadt, Jahr
#VehicleCategory
#VehicleYear
#CampaignID