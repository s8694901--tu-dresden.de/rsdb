# RSDB



## Ziel

Das Ziel dieser Repository ist es, eine einfach zu bedienende Oberfläche für die Remote Sensing-Daten anlässlich des CARES-Projekts aufzubauen.
Die Oberfläche basiert auf Grundlage von Streamlit, eines pythonbasierten Open Source-Framework für Data Science-Apps. Diese wird künftig auf der ZIH-Infrastrukur 
parallel zur Postgres-Datenbank laufen und durch Funktionen ergänzt.

## Voraussetzungen
Es werden nachfolgende Pakete vorausgesetzt (ideal über virtuelle Umgebungen via Anaconda oder einer IDE wie Pycharm):


```
pandas, SQLAlchemy, streamlit
```

Zudem ist eine Verbindung über das Netzwerk der TUD (oder per VPN) notwendig.

## Starten/Testen der App auf dem lokalen Rechner

Die Oberfläche lässt sich nur über die Eingabeaufforderung starten. Erforderlich ist hierfür eine fertig eingerichtete Anaconda-Umgebung oder ähnliches.
Starten lässt sich die Oberfläche mit folgendem Befehl:

```
streamlit run test.py
```

Normalerweise öffnet sich automatisch ein Fenster im Browser. Ansonsten den Link in der Kommandozeile folgen (Standard: (http://localhost:8501))

## Was zu tun ist
~~* Filterfunktionen ausweiten bzw. Bugfixes durchführen~~
~~* Funktionsweise für Performance ausbauen~~
  ~~* Stand 12/22: Laden aller Zeilen mit voreingestellten Filterkriterien~~
  ~~* Wunsch: Laden von max. 500 Zeilen zur Vorschau mit voreingestellten Filterkriterien~~
  ~~* Lade erst alle Zeilen, wenn Downloadbutton betätigt~~
~~* Download im XLSX-Format~~
~~* Kampagnen in Multiselect ausführen~~
~~* Klassifizierung der Kampagnen (Land, Stadt, Ort, Jahr) - in Arbeit (09.01.23)~~
~~* VehicleCategory als Filter~~
* Instrumenten als Filter
* VehicleMake als Spalte mit reinnehmen - 24.01.23 wie?
* Tickbox für Spalten, die zusätzlich ausgegeben werden sollen (Expertenmodus)
* "Explorationsmodus"
* VehiclePassageID, die nicht auswertbar ist, korrigierbar in der DB
* Export in CSV: Datumsformat
* Information zur SiteName (optional)
~~* über Docker auf der TU Research Cloud VM deployen~~